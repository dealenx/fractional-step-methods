#pragma once

#define _USE_MATH_DEFINES
#pragma warning(disable : 4996) //_CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <direct.h>

#include <iomanip>
#include <ctime>
#include <sstream>

using namespace std;

/*
*
* ����� ��������� ���������� �������� (���)
* Alternative Direction Implicit (ADI)
*
*/
class ADIMethod
{
public:

	int N; // ����������� �� x
	int nPlus; // N + 1
	int M; // ����������� �� y
	int mPlus; // M + 1

	double k1;
	double k2;

	double a, b;
	double c, d;

	int timeCount;
	double tau, endTime, timeCurrent;
	double *times;

	double *errorArr;

	double kappaX, kappaY;

	double hX;
	double hY;

	double *x;
	double *y;
	


	double **U; // U �� n ����
	double **uHalf; // U �� n + 1/2 ����
	double **uNext; // U �� n + 1 ����
	double **uAnalitic; // U �� n + 1 ����

	string beginDate;
	string folderName;

	ADIMethod();
	~ADIMethod();

	void init();

	void setBondaryU(double time);

	void solving();

	void firstStep(double time);

	void secondStep(double time);

	double analiticFunc(double t, double x, double y, double k1, double k2);

	double func(double t, double x, double y, double k1, double k2);

	double getNorm(double **A, double **B, int localN, int localM);

	void writeU(double **UForWrite, int localN, int localM);

	void solveThreeDiffMatrixMethod(int n, double * boundary, double * A, double * B, double * C, double * F, double * res);

	void initDate();

	void saveTecplotFile(string preName, double timeVal, double **U, double **uAnalitic, int localN, int localM, double hX, double hY);

	void  saveChartTecplot(string preName, double *timeArr, double *errorArr, int localT);
};

