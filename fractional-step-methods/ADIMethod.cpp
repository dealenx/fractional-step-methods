#include "ADIMethod.h"

ADIMethod::ADIMethod()
{
	initDate();
	init();
	solving();

}

ADIMethod::~ADIMethod()
{

}

double  ADIMethod::func(double x, double y, double t, double k1, double k2) {
	return sin(k1*x)*cos(k2*y)*(cos(t) + (pow(k1, 2) + pow(k2, 2))*sin(t));
}

double  ADIMethod::analiticFunc(double x, double y, double t, double k1, double k2) {
	return sin(k1*x)*cos(k2*y)*sin(t);
}


/*
*=============================
* ИНИЦИАЛИЗАЦИЯ
* ============================
*/

void ADIMethod::init()
{
	a = 0.0;
	b = 1.0;

	c = 0.0;
	d = 1.0;

	N = 50;
	M = N;

	k1 = 1.0;
	k2 = 1.0;

	nPlus = N + 1;
	mPlus = M + 1;

	hX = (b - a) / N;
	hY = (d - c) / M;

	timeCurrent += 0;

	tau = hX / 8.0;
	kappaX = tau / pow(hX, 2);
	kappaY = tau / pow(hY, 2);

	timeCount = 50;
	endTime = tau * timeCount;

	folderName = "N=" + to_string(N) + "_T=" + to_string(timeCount) + "_TAU=" + to_string(tau) + "_DATE=" + beginDate;

	x = new double[nPlus];
	y = new double[mPlus];
	times = new double[timeCount + 1];
	errorArr = new double[timeCount + 1];

	for (int i = 0; i < nPlus; i++)
		x[i] = a + i * hX;

	for (int j = 0; j < mPlus; j++)
		y[j] = c + j * hY;

	for (int j = 0; j < timeCount + 1; j++)
		times[j] = timeCurrent + j * tau;

	U = new double*[nPlus];
	uHalf = new double*[nPlus];
	uNext = new double*[nPlus];
	uAnalitic = new double*[nPlus];

	for (int i = 0; i < nPlus; i++)
	{
		U[i] = new double[mPlus];
		uHalf[i] = new double[mPlus];
		uNext[i] = new double[mPlus];
		uAnalitic[i] = new double[mPlus];
	}

	for (int i = 1; i < N; i++)
	{
		for (int j = 1; j < M; j++)
		{
			U[i][j] = 0.0;
			uHalf[i][j] = 0.0;
			uNext[i][j] = 0.0;
			uAnalitic[i][j] = 0.0;
		}
	}

	for (int i = 1; i < N; i++) {
		for (int j = 1; j < M; j++)
		{
			U[i][j] = 0;
		}
	}

	for (int i = 0; i < N; i++) {
		for (int j = 1; j < M; j++)
		{
			uAnalitic[i][j] = analiticFunc(x[i], y[j], timeCurrent, k1, k2);
		}
	}

	/// Для первого момента времени
	setBondaryU(times[0]);

}


void ADIMethod::setBondaryU(double timeVal) {
	for (int i = 0; i < N + 1; i++) {
		U[i][0] = analiticFunc(x[i], y[0], timeVal, k1, k2);
		uHalf[i][0] = analiticFunc(x[i], y[0], timeVal, k1, k2);
		uNext[i][0] = analiticFunc(x[i], y[0], timeVal, k1, k2);

		U[i][N] = analiticFunc(x[i], y[N], timeVal, k1, k2);
		uHalf[i][N] = analiticFunc(x[i], y[N], timeVal, k1, k2);
		uNext[i][N] = analiticFunc(x[i], y[N], timeVal, k1, k2);
	}
	for (int j = 0; j < M + 1; j++) {
		U[0][j] = analiticFunc(x[0], y[j], timeVal, k1, k2);
		uHalf[0][j] = analiticFunc(x[0], y[j], timeVal, k1, k2);
		uNext[0][j] = analiticFunc(x[0], y[j], timeVal, k1, k2);

		U[N][j] = analiticFunc(x[N], y[j], timeVal, k1, k2);
		uHalf[N][j] = analiticFunc(x[N], y[j], timeVal, k1, k2);
		uNext[N][j] = analiticFunc(x[N], y[j], timeVal, k1, k2);
	}
}

/*
*=============================
* РАСЧЕТ
* ============================
*/

void ADIMethod::solving()
{

	for (int t = 1; t < timeCount; t++) {
		setBondaryU(times[t]);

		firstStep(times[t]);
		secondStep(times[t]);

		for (int i = 0; i < N + 1; i++)
			for (int j = 0; j < M + 1; j++)
				U[i][j] = uNext[i][j];

		for (int i = 0; i < nPlus; i++) {
			for (int j = 0; j < mPlus; j++)
			{
				uAnalitic[i][j] = analiticFunc(x[i], y[j], times[t], k1, k2);
			}
		}

		errorArr[t] = getNorm(U, uAnalitic, nPlus, mPlus);
		cout << "Time[" << t << "]: " << times[t] << ", Norm = " << errorArr[t] << endl;
		saveTecplotFile("U", times[t], U, uAnalitic, nPlus, mPlus, hX, hY);

	
	} // end loop of time

	saveChartTecplot("Chart", times, errorArr, timeCount);
}


void ADIMethod::firstStep(double timeVal)
{
	double *A = new double[N + 1];
	double *B = new double[N + 1];
	double *C = new double[N + 1];
	double *F = new double[N + 1];
	double *Utemp = new double[N + 1];
	double *Res = new double[N + 1];

	for (int i = 1; i < N; i++)
	{
		// Дробных шагов 
		/*
		A[i] = -1.0 / (hX*hX);
		B[i] = 1.0 / tau + 2.0 / (hX*hX);
		C[i] = -1.0 / (hX*hX);
		*/

		// ППП
		A[i] = -kappaX / 2.0;
		B[i] = 1.0 + kappaX;
		C[i] = -kappaX / 2.0;
	}

	for (int j = 1; j < M; j++)
	{
		for (int i = 1; i < N; i++)
		{
			// Дробных шагов
			//F[i] = ( U[i][j] / tau ) + func(x[i], y[j], timeVal, k1, k2);

			//ППП
			
			//F[i] = (kappaY / 2.0)*(U[i][j + 1] - 2 * U[i][j] + U[i][j - 1]) + U[i][j] + (tau / 2.0)*func(x[i], y[j], timeVal - tau, k1, k2);
			F[i] = (kappaY / 2.0)*(U[i][j + 1] - 2 * U[i][j] + U[i][j - 1]) + U[i][j] + (tau / 2.0)*func(x[i], y[j], timeVal, k1, k2);
		}

		for (int i = 0; i < N + 1; i++)
		{
			Utemp[i] = uHalf[i][j];
		}

		solveThreeDiffMatrixMethod(N, Utemp, A, B, C, F, Res);

		for (int i = 0; i < N + 1; i++)
		{
			uHalf[i][j] = Res[i];
		}
	}

}

void ADIMethod::secondStep(double timeVal)
{
	double *A = new double[M + 1];
	double *B = new double[M + 1];
	double *C = new double[M + 1];
	double *F = new double[M + 1];
	double *Utemp = new double[M + 1];
	double *Res = new double[M + 1];

	for (int j = 1; j < M; j++)
	{
		/*
		//Дробных шагов 
		A[j] = -1.0 / (hX*hX);
		B[j] = 1.0 / tau + 2.0 / (hX*hX);
		C[j] = -1.0 / (hX*hX);
		*/


		// ППП
		A[j] = -kappaY / 2.0;
		B[j] = 1.0 + kappaY;
		C[j] = -kappaY / 2.0;
		
	}

	for (int i = 1; i < N; i++)
	{
		for (int j = 1; j < M; j++)
		{
			//Дробных шагов
			//F[j] = (uHalf[i][j] / tau) + func(x[i], y[j], timeVal, k1, k2);
			
			//ППП
			//F[j] = (kappaX / 2.0)*(uHalf[i + 1][j] - 2 * uHalf[i][j] + uHalf[i - 1][j]) + uHalf[i][j] + (tau / 2.0)*func(x[i], y[j], timeVal - (tau / 2), k1, k2);
			F[j] = (kappaX / 2.0)*(uHalf[i + 1][j] - 2 * uHalf[i][j] + uHalf[i - 1][j]) + uHalf[i][j] + (tau / 2.0)*func(x[i], y[j], timeVal, k1, k2);
		}

		for (int j = 0; j < M + 1; j++)
		{
			Utemp[j] = uNext[i][j];
		}

		solveThreeDiffMatrixMethod(M, Utemp, A, B, C, F, Res);

		for (int j = 0; j < M + 1; j++)
		{
			uNext[i][j] = Res[j];
		}
	}
}


/**
 * ========================================================
 * Метод разностной прогонки трехдиагональной матрицы
 * n - размерность
 * boundary - краевое условие
 * C - верхняя диагональ
 * B - Средняя диагональ
 * A - Нижняя диагональ
 * F -правая часть
 * res - Возвращаемый результат
 * ========================================================
 */
void ADIMethod::solveThreeDiffMatrixMethod(int n, double * boundary, double * A, double * B, double * C, double * F, double * res)
{
	double *alpha = new double[n];
	double *beta = new double[n];

	for (int i = 0; i < n + 1; i++)
	{
		res[i] = boundary[i];
	}

	for (int i = 1; i < n; i++)
	{
		alpha[0] = 0;
		beta[0] = res[0];

		alpha[i] = -A[i] / (B[i] + C[i] * alpha[i - 1]);
		beta[i] = (F[i] - C[i] * beta[i - 1]) / (B[i] + C[i] * alpha[i - 1]);
	}

	for (int i = n - 1; i >= 1; i--)
	{
		res[i] = alpha[i] * res[i + 1] + beta[i];
	}
}


double  ADIMethod::getNorm(double **A, double **B, int localN, int localM) {
	double maxVal = fabs(A[0][0] - B[0][0]);

	for (int i = 0; i < localN; i++)
	{
		for (int j = 0; j < localM; j++)
		{
			if (maxVal < fabs(A[i][j] - B[i][j])) {
				maxVal = fabs(A[i][j] - B[i][j]);
			}
		}
	}

	return maxVal;
}

void  ADIMethod::writeU(double **UForWrite, int localN, int localM) {
	for (int i = 0; i < localN; i++)
	{
		for (int j = 0; j < localM; i++)
		{
			cout << UForWrite[i][j] << " ";
		} cout << endl;
	}
}


/*
*=============================
* СОХРАНЕНИЕ ДЛЯ TECPLOT
* ============================
*/

void ADIMethod::initDate()
{
	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);

	std::ostringstream oss;
	oss << std::put_time(&tm, "%d-%m-%Y %H-%M-%S");
	auto str = oss.str();

	beginDate = str;
}

void  ADIMethod::saveTecplotFile(string preName, double timeVal, double **U, double **uAnalitic, int localN, int localM, double hX, double hY)
{
	string fileName = preName + "_" + to_string(timeVal) + ".dat";
	string parentFolder = "./data/";
	string folderPath = parentFolder + folderName + "/";
	string filePath = folderPath + fileName;

	_mkdir(parentFolder.c_str());
	_mkdir(folderPath.c_str());

	ofstream fout;
	fout.open(filePath);
	fout << "TITLE=\"ADI\"" << endl;
	fout << "VARIABLES=\"X\",\"Y\",\"U\",\"uAnalitic\"" << endl;
	fout << "ZONE T=\"" << timeVal << "\", F=POINT" << ", I= " << localN << " , J=" << localM << endl;

	for (int i = 0; i < localN; i++)
	{
		for (int j = 0; j < localM; j++)
		{
			fout << (a + i * hX) << "\t" << (c + j * hY) << "\t" << U[i][j] << "\t" << uAnalitic[i][j] << "\t" << endl;
		}
	}

	fout.close();
}

void  ADIMethod::saveChartTecplot(string preName,  double *timeArr, double *errorArr, int localT)
{
	string fileName = preName +  ".dat";
	string parentFolder = "./data/";
	string folderPath = parentFolder + folderName + "/";
	string filePath = folderPath + fileName;

	_mkdir(parentFolder.c_str());
	_mkdir(folderPath.c_str());

	ofstream fout;
	fout.open(filePath);
	fout << "TITLE=\"Chart\"" << endl;
	fout << "VARIABLES=\"time\",\"error\"" << endl;

	for (int t = 1; t < localT; t++)
	{
			fout << timeArr[t] << "\t" << errorArr[t] << endl;
	}

	fout.close();
}
